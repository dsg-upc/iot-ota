#include "loramesher.h"
#include "otaWebServer.h"

#pragma region WebServerConfig
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

bool ledState = 0;
const int ledPin = 4;

// Replace with your network credentials
const char* ssid = "Fibracat_21123";
const char* password = "85392c7e38";

const char* userName = "Hello";
const char* userPassword = "vamoahi";

#pragma endregion

#pragma region LoraMesher

LoraMesher& radio = LoraMesher::getInstance();

uint32_t dataCounter = 0;
struct dataPacket {
  uint32_t counter = 0;
};

dataPacket* helloPacket = new dataPacket;
/**
 * @brief Print the counter of the packet
 *
 * @param data
 */
void printPacket(dataPacket data) {
  Log.verbose(F("Hello Counter received nº %X" CR), data.counter);
}

/**
 * @brief Iterate through the payload of the packet and print the counter of the packet
 *
 * @param packet
 */
void printDataPacket(LoraMesher::userPacket<dataPacket>* packet) {
  Log.trace(F("Packet arrived from %X with size %d" CR), packet->src, packet->payloadSize);

  //Get the payload to iterate through it
  dataPacket* dPacket = packet->payload;
  size_t payloadLength = radio.getPayloadLength(packet);

  for (size_t i = 0; i < payloadLength; i++) {
    //Print the packet
    printPacket(dPacket[i]);
  }
}

/**
 * @brief Function that process the received packets
 *
 */
void processReceivedPackets(void*) {
  for (;;) {
    /* Wait for the notification of processReceivedPackets and enter blocking */
    ulTaskNotifyTake(pdPASS, portMAX_DELAY);

    //Iterate through all the packets inside the Received User Packets FiFo
    while (radio.getReceivedQueueSize() > 0) {
      Log.trace(F("ReceivedUserData_TaskHandle notify received" CR));
      Log.trace(F("Fifo receiveUserData size: %d" CR), radio.getReceivedQueueSize() > 0);

      //Get the first element inside the Received User Packets FiFo
      LoraMesher::userPacket<dataPacket>* packet = radio.getNextUserPacket<dataPacket>();

      //Print the data packet
      printDataPacket(packet);

      //Delete the packet when used. It is very important to call this function to release the memory of the packet.
      radio.deletePacket(packet);
    }
  }
}

/**
 * @brief Every 20 seconds it will send a counter to a position of the dataTable
 *
 */
void sendLoRaMessage(void*) {
  int dataTablePosition = 0;

  for (;;) {
    // if (radio.routingTableSize() == 0) {
    //   vTaskDelay(20000 / portTICK_PERIOD_MS);
    //   continue;
    // }

    // if (radio.routingTableSize() <= dataTablePosition)
    //   dataTablePosition = 0;

    // uint16_t addr = radio.routingTable[dataTablePosition].networkNode.address;

    Log.trace(F("Send data packet nº %d to %X (%d)" CR), dataCounter, "0x0000", dataTablePosition);

    dataTablePosition++;

    //Create packet and send it.
    radio.createPacketAndSend(0x0000, helloPacket, 1);

    //Increment data counter
    helloPacket->counter = dataCounter++;

    //Wait 20 seconds to send the next packet
    vTaskDelay(20000 / portTICK_PERIOD_MS);
  }
}

/**
 * @brief Setup the Task to create and send periodical messages
 *
 */
void createSendMessages() {
  TaskHandle_t sendLoRaMessage_Handle = NULL;
  BaseType_t res = xTaskCreate(
    sendLoRaMessage,
    "Send LoRa Message routine",
    4098,
    (void*) 1,
    1,
    &sendLoRaMessage_Handle);
  if (res != pdPASS) {
    Log.error(F("Send LoRa Message task creation gave error: %d" CR), res);
    vTaskDelete(sendLoRaMessage_Handle);
  }
}

void setupLoraMesher() {
  //Init the loramesher with a processReceivedPackets function
  radio.init(processReceivedPackets);

  //Init send messages task
  createSendMessages();

  Serial.println("Lora initialized");
}

#pragma endregion

void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);

  initOta(&server, &ws, ssid, password, userName, userPassword);
  setupLoraMesher();

}

void loop() {
  ws.cleanupClients();
  digitalWrite(ledPin, ledState);
}