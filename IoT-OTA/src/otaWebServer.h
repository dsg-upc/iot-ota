#ifndef __otaWebServer__
#define __otaWebServer__

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

void initOta(AsyncWebServer* server, AsyncWebSocket* ws, const char* ssid, const char* password, const char* userName, const char* userPassword);

void notifyClients();

void handleWebSocketMessage(void* arg, uint8_t* data, size_t len);

void onEvent(AsyncWebSocket* server, AsyncWebSocketClient* client, AwsEventType type,
    void* arg, uint8_t* data, size_t len);

void initWebSocket(AsyncWebServer* server, AsyncWebSocket* ws);

String processor(const String& var);

#endif